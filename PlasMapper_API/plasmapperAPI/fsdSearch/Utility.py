from abc import ABC
from decouple import config
import os, csv, random, time
from Bio.Blast.Applications import NcbiblastnCommandline
import optipyzer
from django.core.mail import send_mail
from django.conf import settings


class Utility(ABC): # abstract class
    # Returns the value of the specified key in .env
    def getEnvValue(key):
        return config(key)

def runDiamondBlast(querySeq):
    DIAMOND_BASE_PATH = "/home/sukanta/plasmapper/ver4/plasmapper3/PlasMapper_API/plasmapperAPI/fsdSearch/DIAMOND/"
    DIAMOND_CMD = f"{DIAMOND_BASE_PATH}diamond"
    DB_NAME = "testDB.dmnd"
    DB_PATH = f"{DIAMOND_BASE_PATH}{DB_NAME}"
    BLAST_TOOL = "blastx"
    NUM_THREADS = 4
    QUERY_TASKS_PATH = f"{DIAMOND_BASE_PATH}queryTasks/"
    OUT_FMT = "6 stitle pident length evalue bitscore full_sseq"
    OUTFMT_COLS = ["subjectTitle", "percentIdentity", "length", "evalue", "bitscore", "fullSubjectSeq"]

    # create input/output file with querySeq
    taskID = f"{random.randint(100000, 999999)}"
    print("taskID:", taskID)
    inputTextFilePath = f"{QUERY_TASKS_PATH}{taskID}-querySeq.fasta"
    os.system(f"touch {inputTextFilePath}")
    os.system(f'echo ">query\n{querySeq}" > {inputTextFilePath}')
    outputCsvFilePath = f"{QUERY_TASKS_PATH}{taskID}-hits.tsv"
    os.system(f"touch {outputCsvFilePath}")

    try:
        cmd = f"{DIAMOND_CMD} {BLAST_TOOL} -d {DB_PATH} -q {inputTextFilePath} -o {outputCsvFilePath} -f {OUT_FMT} --fast"
        os.system(cmd)

        res = {'meta': {'taskID': taskID, 'totalHits': 0}, 'hits': []}
        with open(outputCsvFilePath) as outFh:
            outFh = outFh.readlines()
            res['meta']['totalHits'] = len(outFh)
            for hit in outFh:
                hitObj = {}
                hit = hit.split('\t')
                for i in range(len(OUTFMT_COLS)):
                    hitObj[OUTFMT_COLS[i]] = hit[i]
                res['hits'].append(hitObj)

        return res
    except Exception as e:
        print(e)
        return {
            "status": "Blast-ERROR-runDiamondBlast",
            "error": str(e)
        }

def runNCBIBlast(querySeq, email, remote):
    print("IMPORTED~~~runNCBIBlastRemote")

    # change to prod
    # NCBI_BLAST_BIN_BASE = f"/home/sukanta/plasmapper/ver4/plasmapper3/PlasMapper_API/plasmapperAPI/fsdSearch/ncbi-blast-2.12.0+/bin/"
    NCBI_BLAST_BIN_BASE = f"/root/project/plasmapper3/PlasMapper_API/plasmapperAPI/fsdSearch/ncbi-blast-2.12.0+/bin/"  # production

    # change to prod
    # BLAST_DB_PATH = "./fsdSearch/ncbi-blast-2.12.0+/bin/16S_ribosomal_RNA"  # sukanta's dev
    BLAST_DB_PATH = "/mnt/disks/blast/downloads/ncbi-blast-2.13.0+/bin/nt"  # production

    BLAST_TOOL_TYPE = "blastn"
    BLAST_CMD = f"{NCBI_BLAST_BIN_BASE}{BLAST_TOOL_TYPE}"

    OUTPUT_FMT = "10 delim=@@ stitle score bitscore evalue qstart qend pident"
    OUTPUT_FMT_COLUMNS = ["subjectTitle", "rawScore", "bitScore", "evalue", "queryStart", "queryEnd", "percentIdentity"]
    QUERY_TASKS_FILEPATH = f"{NCBI_BLAST_BIN_BASE}queryTasks/"

    try:
        # create input file with querySeq
        taskID = f"{random.randint(100000, 999999)}"
        print("taskID:", taskID)
        inputTextFilePath = f"{QUERY_TASKS_FILEPATH}{taskID}-querySeq.txt"
        os.system(f"touch {inputTextFilePath}")
        os.system(f'echo "{querySeq}" > {inputTextFilePath}')

        # run blast
        # taskID = 489001
        outputCsvFilePath = f"{QUERY_TASKS_FILEPATH}{taskID}-hits.csv"
        os.system(f"touch {outputCsvFilePath}")

        command = NcbiblastnCommandline(cmd=BLAST_CMD, db=BLAST_DB_PATH, query=inputTextFilePath, out=outputCsvFilePath, outfmt=OUTPUT_FMT, remote=remote)
        print("blastn command:", command)
        print(f"[runNCBIBlastRemote]: Query with taskID {taskID} sent to remote server. Waiting for reply...")
        command()
        print("[runNCBIBlastRemote]: Reply recieved.")

        # time.sleep(3)
        # print(os.listdir(QUERY_TASKS_FILEPATH))
        fh = open(outputCsvFilePath)
        hits = fh.readlines()
        res = {
            "meta": {
                "taskID": taskID,
                "totalHits": len(hits)
            },
            "blastHits": []
        }
        if len(hits) == 0:
            res["blastHits"] = [{"subjectTitle": "no blast hits found"}]
        else:
            j = 0
            for row in hits:
                allCols = {"#_hit": str(j+1)}
                j += 1
                row = row.strip().split('@@')
                for i in range(len(OUTPUT_FMT_COLUMNS)):
                    allCols[OUTPUT_FMT_COLUMNS[i]] = row[i]
                res["blastHits"].append(allCols)
                if j >= 50: break
        fh.close()

        print("res", res)
        print("email", email)
        emailBlastHits(email, res, querySeq)

        return res

    except Exception as e:
        print("BLAST ERROR:", e)
        return {
            "status": "Blast-ERROR-runNCBIBlast",
            "error": str(e)
        }


def emailBlastHits(email, res, querySeq):
    subject = "PlasMapper3.0 - BLAST Results"

    message = "Hi! Your BLAST results from PlasMapper3.0 are ready!\n"
    message += f"\nBLAST Hits:\n"
    for k in res["blastHits"][0].keys():
        message += f"{k},"

    message += "\n"
    for h in res["blastHits"]:
        for k in h.keys():
            message += f"{h[k]},"
        message += "\n"

    message += "No BLAST hits found."
    message += f"\nQuery Sequence:\n{querySeq}\n"

    send_mail(subject, message, from_email="app@wishartlab.com", recipient_list=[email], fail_silently=False)



# def runNCBIBlast(querySeq):
#     print("IMPORTED~~~")

    # these are currently set to sukanta's local paths. must change to production
#     NCBI_BLAST_BIN_BASE = f"/home/sukanta/plasmapper/ver4/plasmapper3/PlasMapper_API/plasmapperAPI/fsdSearch/ncbi-blast-2.12.0+/bin/"
#     BLAST_TOOL_TYPE = "blastn"
#     BLAST_CMD = f"{NCBI_BLAST_BIN_BASE}{BLAST_TOOL_TYPE}"
#     BLAST_DB_NAME = "GCF_000001405.39_top_level"
#     BLAST_DB_PATH = f"{NCBI_BLAST_BIN_BASE}{BLAST_DB_NAME}"
#     NUM_ALIGNMENTS = 10
#     NUM_THREADS = 2
#     OUTPUT_FMT = "10 delim=@@ stitle score bitscore evalue qstart qend pident"
#     OUTPUT_FMT_COLUMNS = ["subjectTitle", "rawScore", "bitScore", "evalue", "queryStart", "queryEnd", "percentIdentity"]
#     QUERY_TASKS_FILEPATH = f"{NCBI_BLAST_BIN_BASE}queryTasks/"

#     try:
#         # create input file with querySeq
#         taskID = f"{random.randint(100000, 999999)}"
#         print("taskID:", taskID)
#         inputTextFilePath = f"{QUERY_TASKS_FILEPATH}{taskID}-querySeq.txt"
#         os.system(f"touch {inputTextFilePath}")
#         os.system(f'echo "{querySeq}" > {inputTextFilePath}')

#         # run blast
#         outputCsvFilePath = f"{QUERY_TASKS_FILEPATH}{taskID}-hits.csv"
#         os.system(f"touch {outputCsvFilePath}")

#         command = NcbiblastnCommandline(cmd=BLAST_CMD, db=BLAST_DB_PATH, query=inputTextFilePath, out=outputCsvFilePath, outfmt=OUTPUT_FMT, num_alignments=NUM_ALIGNMENTS)
#         # print(command)
#         command()

#         # time.sleep(3)

#         # print(os.listdir(QUERY_TASKS_FILEPATH))
#         fh = open(outputCsvFilePath)
#         hits = fh.readlines()
#         res = {"meta": {"totalHits": len(hits)}, "blastHits": []}
#         if len(hits) == 0:
#             res["blastHits"] = [{"subjectTitle": "no blast hits found"}]
#         else:
#             j = 0
#             for row in hits:
#                 allCols = {"#_hit": str(j+1)}
#                 j += 1
#                 row = row.strip().split('@@')
#                 for i in range(len(OUTPUT_FMT_COLUMNS)):
#                     allCols[OUTPUT_FMT_COLUMNS[i]] = row[i]

#                 res["blastHits"].append(allCols)
#         fh.close()

#         print("res", res)

#         return res

#     except Exception as e:
#         print("BLAST ERROR:", e)
#         return {
#             "status": "Blast-ERROR-runNCBIBlast",
#             "error": str(e)
#         }



def doCodonOptimization(seq, targetSpecies):
    ORG_ID = {  # IDs refer to the org_id column from table autocomplete_data in optipyzer's codon_usage_data.db file (sqlite)
        "E. Coli (Escherichia coli str. K-12 substr. MG1655)": "16815",
        "Bakers Yeast (Saccharomyces cerevisiae S288C)": "121713",
        "Caenorhabditis elegans": "122001",
        "Fruit Fly (Drosophila melanogaster)": "122056",
        "Thale Cress (Arabidopsis thaliana)": "122263",
        "Human (Homo sapiens)": "122563",
        "Mouse (Mus musculus)": "122638",
        "Rat (Rattus norvegicus)": "122645",
        "Zebrafish (Danio rerio)":"122731",
        "African Clawed Frog (Xenopus laevis)":"122771"
    }

    try:
        querySeq = seq[:(len(seq)//3)*3]  # seq length must be divisible by 3 for optipyzer
        straySeq = seq[len(seq)//3*3:]
        op = optipyzer.api(local=True)
        print("Initialised optipyzer. Starting query...")
        opRes = op.optimize(
            querySeq,
            {ORG_ID[targetSpecies]: 1},
            seq_type="dna",
            seed=1234,
            iterations=1000
        )
        print("Optimization done.")
        # print(opRes)
        return {"targetSpecies": targetSpecies,
                "targetSpeciesID": ORG_ID[targetSpecies],
                "originalSeq": seq,
                "res": opRes,
                "straySeq": straySeq
            }

    except Exception as e:
        print("OPTIPYZER ERROR:", e)
        return {
            "status": "optipyzer-ERROR-doCodonOptimization",
            "error": str(e),
            "inputSeq": seq
        }
