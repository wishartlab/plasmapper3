{
  "version": "1.2",
  "dbname": "features.fasta.nt",
  "dbtype": "Nucleotide",
  "db-version": 5,
  "description": "features.fasta.nt",
  "number-of-letters": 195772,
  "number-of-sequences": 471,
  "last-updated": "2022-12-04T21:48:00",
  "number-of-volumes": 1,
  "bytes-total": 155070,
  "bytes-to-cache": 55021,
  "files": [
    "features.fasta.nt.ndb",
    "features.fasta.nt.nhr",
    "features.fasta.nt.nin",
    "features.fasta.nt.not",
    "features.fasta.nt.nsq",
    "features.fasta.nt.ntf",
    "features.fasta.nt.nto"
  ]
}
