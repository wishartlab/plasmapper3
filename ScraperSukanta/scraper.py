from bs4 import BeautifulSoup as bs
from requests import get
# from urllib.requests import urlopen

page = get("https://www.addgene.org/browse/sequence_vdb/2093/")
soup = bs(page.content)
soup = soup.find('iframe', {'id': 'giraffe-analyze-frame'})

src = soup.attrs['src']
print("Iframe SRC:", src)

dataPage = get(f"http://addgene.org{src}")
dataSoup = bs(dataPage.content, 'lxml')
dataSoup = dataSoup.find('div', {'id': 'results_div'})
print(dataSoup)



# for iframe in iframexx:
#     response = urllib2.urlopen(iframe.attrs['src'])
#     iframe_soup = BeautifulSoup(response)
