import requests
import json


# creating json file from .txt file
sequences = []
with open("yeast.txt") as fh:
    fh = fh.readlines()
    for i in range(len(fh)):
        print("here1")
        plasmid = {}
        # print(fh[i][0])
        if fh[i][0] == ">":
            # print("here2", i, fh[i])
            plasmid['name'] = fh[i].replace("\n", "").replace(">", "")
            seq = ""
            # print(fh[i+1][0])
            j = 1
            k = i
            while not fh[k+j][0] == "\n":
                seq += fh[k+j].replace("\n", "")
                j += 1
            plasmid['sequence'] = seq
            sequences.append(plasmid)
        # break


print(sequences)


def addSequences():
    with open("test.json", 'w') as fh:
        with open("../PlasMapper_Webscraper/plasmidDB/sequences.json") as fh2:
            existing_dict = json.load(fh2)
            existing_dict += sequences
            fh = json.dump(existing_dict, fh)
            # print(fh)

# addSequences()

def addMetadata():
    cols = ["name", "sequenceLength", "backbone", "features", "supplier", "expression", "popularity"]
    newMetadata = []
    with open("testMeta.json", 'w') as fh:
        for plasmid in sequences:
            plasmidMetadata = {}
            plasmidMetadata["name"] = plasmid["name"]
            plasmidMetadata["sequenceLength"] = len(plasmid["sequence"])
            plasmidMetadata["backbone"] = plasmid["name"]
            plasmidMetadata["features"] = ""
            plasmidMetadata["supplier"] = "GeneScript"
            plasmidMetadata["expression"] = "Mammalian Expression"
            plasmidMetadata["popularity"] = 0
            newMetadata.append(plasmidMetadata)

        with open("../PlasMapper_Webscraper/plasmidDB/metadata.json") as fh2:
            existing_meta = json.load(fh2)
            existing_meta += newMetadata
            fh = json.dump(existing_meta, fh)


        # fh = json.dump(allMetadata, fh)

# addMetadata()

def createSeqJson(expression):
    sequences = []
    with open(f"{expression}.txt", "r") as fh:
        fh = fh.readlines()
        for i in range(len(fh)):
            plasmid = {}
            # print(fh[i][0])
            if fh[i][0] == ">":
                plasmid["name"] = fh[i].replace("\n", "").replace(">", "")
                seq = ""
                # print(fh[i+1][0])
                j = 1
                k = i
                if fh[k+j][0]:
                    # print(fh[k+j])
                    while not fh[k+j][0] == "\n":
                        seq += fh[k+j].replace("\n", "")
                        j += 1
                    plasmid["sequence"] = seq
                sequences.append(plasmid)

    # print(sequences)

    with open(f"{expression}.json", 'w') as fh2:
        # fh2.write(str(sequences))
        json.dump(sequences, fh2)


# createSeqJson("mammalian")

# expressions = ["mammalian", "yeast"]
# with open('newSequences2.json', 'w') as fh:
#     allSeqs = []
#     for exp in expressions:
#         with open(f"{exp}.json") as fh2:
#             allSeqs += json.load(fh2)
#     json.dump(allSeqs, fh)





expressionTypes = ['Yeast Expression', 'Plant Expression', 'Bacterial Expression', 'Mammalian Expression', 'Insect Expression', 'Worm Expression', 'CRISPR']
def createMetadata(expression):
    cols = ["name", "sequenceLength", "backbone", "features", "supplier", "expression", "popularity"]
    newMetadata = []
    with open(f"{expression}.json") as fh:
        sequences = json.load(fh)

        for plasmid in sequences:
            plasmidMetadata = {}
            plasmidMetadata["name"] = plasmid["name"]
            plasmidMetadata["sequenceLength"] = len(plasmid["sequence"])
            plasmidMetadata["backbone"] = plasmid["name"]
            plasmidMetadata["features"] = ""
            plasmidMetadata["supplier"] = "GeneScript"
            plasmidMetadata["expression"] = "Mammalian Expression"
            plasmidMetadata["popularity"] = 0
            newMetadata.append(plasmidMetadata)

    with open(f"{expression}Meta.json", 'w') as fh2:
        json.dump(newMetadata, fh2)


# createMetadata("mammalian")


# expressions = ["mammalian", "yeast"]
# with open('newMetadata2.json', 'w') as fh:
#     allMeta = []
#     for exp in expressions:
#         with open(f"{exp}Meta.json") as fh2:
#             allMeta += json.load(fh2)
#     json.dump(allMeta, fh)





with open("newMetadata3.json") as fh:
    newMeta = json.load(fh)
    with open("../PlasMapper_Webscraper/plasmidDB/metadataBackup4.json") as fh2:
        existing = json.load(fh2)

        both = existing + newMeta
        with open("../PlasMapper_Webscraper/plasmidDB/metadata.json", 'w') as fh3:
            json.dump(both, fh3)

    print("newMeta:", newMeta)


# with open("newSequences3.json") as fh:
#     newMeta = json.load(fh)
#     with open("../PlasMapper_Webscraper/plasmidDB/sequencesBackup4.json") as fh2:
#         existing = json.load(fh2)

#         both = existing + newMeta
#         with open("../PlasMapper_Webscraper/plasmidDB/sequences.json", 'w') as fh3:
#             json.dump(both, fh3)




