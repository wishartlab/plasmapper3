export const TableColumns = [
    { field: 'popularity', headerName: 'Popularity', width: 120 },
    { field: 'backbone', headerName: 'Plasmid Parent', width: 150 },
    { field: 'expression', headerName: 'Expression System', width: 350 },
    { field: 'features', headerName: 'Features', width: 450 },
    { field: 'name', headerName: 'Full Name', width: 250 },
    { field: 'sequenceLength', headerName: 'Length', width: 100 },
    { field: 'supplier', headerName: 'Supplier', width: 100 },
]
