import axios from 'axios'

export default axios.create({
    baseURL: `https://plasmapper.ca/api/`
    // baseURL: 'http://127.0.0.1:7000/api/'
});
