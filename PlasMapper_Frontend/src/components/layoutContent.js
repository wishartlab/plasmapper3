import * as React from "react"

import "../styles/layout.css"
import * as style from "../styles/footer.module.css"

import GlobalContext from "../context/optionContext"

import { Link } from '@mui/material';
import tmic from "../images/tmic.png"
import genomeAlberta from "../images/genomealberta.png"
import genomeCanada from "../images/genomecanada.png"
import innovationCanada from "../images/innovationcanada.png"
import cihrirsc from "../images/cihrirsc.png"


function LayoutContent({children}){
  const {theme, language} = React.useContext(GlobalContext);

  return(
    <div
        style={{...theme, backgroundColor: theme['--tint']}}
        >
          <main>{children}</main>
          <footer
            class={style.footer}
          >
            {language.FOOTER}<a class={style.link} target="_blank" rel="noopener noreferrer" href={"http://feedback.wishartlab.com/?site=plasmapper"}>here</a>
            <div>{language.FOOTER_CREDIT}</div>
            <p>
              This project is supported by the <Link href="https://cihr-irsc.gc.ca/" target="_blank" underline="hover">Canadian Institutes of Health Research</Link>, <Link underline="hover" href="https://www.innovation.ca/" target="_blank">Canada Foundation for Innovation</Link>, and by <Link underline="hover" href="https://metabolomicscentre.ca/" target="_blank">The Metabolomics Innovation Centre (TMIC)</Link>, a nationally-funded research and core facility that supports a wide range of cutting-edge metabolomic studies. TMIC is funded by <Link underline="hover" href="https://genomecanada.ca/" target="_blank">Genome Canada</Link> and <Link underline="hover" href="https://genomealberta.ca/" target="_blank">Genome Alberta</Link>, a not-for-profit organization that is leading Canada's national genomics strategy with $900 million in funding from the federal government.
            </p>
            <div>
              <a href="https://genomecanada.ca/" target="_blank"><img  style={{height: '9em'}} src={genomeCanada}/></a>
              <a href="https://genomealberta.ca/" target="_blank"><img style={{height: '9em'}} src={genomeAlberta}/></a>
              <a href="https://metabolomicscentre.ca/" target="_blank"><img  style={{height: '9em'}} src={tmic}/></a>
              <a href="https://www.innovation.ca/" target="_blank"><img  style={{height: '9em'}} src={innovationCanada}/></a>
              <a href="https://cihr-irsc.gc.ca/" target="_blank"><img  style={{height: '9em'}} src={cihrirsc}/></a>
            </div>
          </footer>
    </div>
  )
}



export default LayoutContent
