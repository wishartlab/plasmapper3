// dense table template from https://mui.com/material-ui/react-table/

import * as React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';


export default function BlastHitsTable(props) {
    const { blastHits } = props
    const [tableData, setTableData] = React.useState(blastHits)

    React.useEffect(() => {
        console.log("blastHits-table: ", blastHits)
        console.log("tableData: ", tableData)
        setTableData(blastHits)
    }, [tableData])

    const blastColsNcbi = ["#", "Gene Title", "Raw Score", "Bit Score", "E-Value", "Plasmid Start", "Plasmid Stop", "Percent Identity"]
    return (
        <TableContainer component={Paper} sx={{ maxHeight: '34em', maxWidth: '30em' }}>
            <Table stickyHeader size="small" aria-label="a dense table">
                <TableHead>
                    <TableRow>
                        <TableCell sx={{ fontWeight: '800' }} align="right">#</TableCell>
                        <TableCell sx={{ fontWeight: '800' }} >Gene Title</TableCell>
                        <TableCell sx={{ fontWeight: '800' }} align="right">Raw Score</TableCell>
                        <TableCell sx={{ fontWeight: '800' }} align="right">Bit Score</TableCell>
                        <TableCell sx={{ fontWeight: '800' }} align="right">E-Value</TableCell>
                        <TableCell sx={{ fontWeight: '800' }} align="right">Plasmid Start</TableCell>
                        <TableCell sx={{ fontWeight: '800' }} align="right">Plasmid Stop</TableCell>
                        <TableCell sx={{ fontWeight: '800' }} align="right">Percent Identity</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {blastHits && blastHits.map((row) => (
                        <TableRow
                            key={row["#_hit"]}
                            sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                            <TableCell align="right">{row["#_hit"]}</TableCell>
                            <TableCell component="th" scope="row">{row.subjectTitle}</TableCell>
                            <TableCell align="right">{row['rawScore']}</TableCell>
                            <TableCell align="right">{row['bitScore']}</TableCell>
                            <TableCell align="right">{row['evalue']}</TableCell>
                            <TableCell align="right">{row['queryStart']}</TableCell>
                            <TableCell align="right">{row['queryEnd']}</TableCell>
                            <TableCell align="right">{row['percentIdentity']}</TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    );
}
