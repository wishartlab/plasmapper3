import * as React from 'react';
import SeqViz from "seqviz";
import { Button, LinearProgress, Typography, Divider, Modal, Box } from "@mui/material";
import { toPng, toSvg } from 'html-to-image';
import Download from '@mui/icons-material/Download';
// import download from 'downloadjs'


export default function PosterViewModal(props) {

    const seqvizRef = React.createRef()

    return (
        <Modal
            open={props.posterView}
            onClose={() => { props.setPosterView(false) }}
            aria-labelledby="plasmapper-poster-view"
            aria-describedby="plasmapper-poster-view"
        >
            <div style={{ position: 'absolute', background: 'white', width: '95%', height: '95%', top: '1em', left: '2.5em', borderRadius: '5px' }}>
                {/* <div style={{ display: 'flex', flexDirection: 'col', width: '100%', height: '100%', background: 'white', borderRadius: '5px' }}> */}
                <div style={{ display: 'flex', flexDirection: 'row' }}>
                    {/* <Button
                        color="secondary"
                        sx={{ margin: '10px' }}
                        onClick={() => {
                            toSvg(document.getElementById('posterViewSeqvizImage'), {})
                                .then(function (dataUrl) {
                                    download(dataUrl, 'test.svg')
                                });
                        }}
                    ><Download sx={{ paddingTop: '3px' }} />Download Map as SVG</Button> */}
                    <Button
                        color="error"
                        sx={{ margin: '10px' }}
                        onClick={() => {
                            props.setPosterView(false)
                        }}
                    >Close</Button>
                </div>
                <div style={{ width: '100%', height: '100%' }} id="posterViewSeqvizImage">
                    <SeqViz
                        seq={props.seq}
                        name={props.name}
                        viewer={props.viewer}
                        enzymes={props.enzymes}
                        annotations={props.annotations}
                    />
                </div>
                {/* </div> */}
            </div>
            {/* <Box>
                <Typography id="modal-modal-title" variant="h6" component="h2">
                    Text in a modal
                </Typography>
                <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                    Duis mollis, est non commodo luctus, nisi erat porttitor ligula.
                </Typography>
            </Box> */}
        </Modal >
    )
}
