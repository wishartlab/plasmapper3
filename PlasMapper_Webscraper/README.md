# PlasMapper_Web_Scraper

## File Descriptions:

### scrapeFeatures.py

#### Contains a function "collectData" that takes in a file name to write to and scrapes all features from the AddGene site. This function is non-transactional in that it will write features to the file as they are scraped. If an error occurs during the scrape, a partially finished text file will exist. It should be noted that this webscraper was built in early 2022, and certain changes to the format of the AddGene website could require that the scraper be refactored before use.

### duplicates.py

#### Contains a function "getDuplicates" that takes in a query file of features in FASTA format, passes each one to the /features endpoint of the API, and then writes the names of any features that resulted in more than one hit (i.e. they have duplicate(s) in the API database). Duplicates are not directly removed from the database by this function because an API hit can result from highly similar, but not identical features. Furthermore, if sequences are in fact identical, it should be investigated which of the duplicate features have a less informative name, and this one should be removed. This file was primarily used when merging the PlasMapper 2.0 feature database with the new webscraped feature database.

### scrapePlasmids.py

#### Contains a function "collectPlasmids" that takes in file names for which to output sequences and meta data of plasmids on the AddGene filtered page specified as a global variable "baseUrl". There are 3 labelled sections in this function that can be commented out to scrape plasmid sequences, meta data, or both. An important point about the AddGene page is that page size is limited to 50 results, but pagination order is not consistent between pages. This means that with one pass through all pages of any result set greater than 50, we are not guaranteed to see each unique result at least once. For example, given a result set of 100 plasmids, the first page of 50 may contain plasmids that are also shown on page 2, meaning that to see all unique results, we must continuously refresh the page to reorder until we have seen 100 unique results. I have not been able to find any consistent pattern to this ordering. That is why I have made this function completely transactional in that if any error occurs during the run, none of the collected results will be written to the file. This way, when restarting after an error, we don't need to deal with any inconsistency in what results are already in the file. The scraper keeps track of the names of plasmids for which a result is already scraped, and then loops over the pages until all unique results have been found. This approach can often take a long time to run depending on the size of your result set, which is why I have broken the total result set into batches using the scheme described in "batches.txt". This file contains the filters used on the AddGene page for each batch, as well as the batch size. It should be noted that this webscraper was built in early 2022, and certain changes to the format of the AddGene website could require that the scraper be refactored before use.

### getPlasmidFeatures.py

#### Contains a function "getPlasmidFeatures.py" that takes in the names of metadata and sequence files, and passes the sequence of each plasmid in the metafile (from the sequence file, names must be equal between the two files) to the API annotation endpoint in order to pre-compute all features for that plasmid. It then writes these features in a comma seperated string of the feature names to the empty "features" attribute in the metafile. This is to support search by feature name functionality. This is a completely transactional function in that no results will be written to the file unless all plasmids have run successfully.

#### This file also contains a function "removeDuplicates" that takes in the names of metadata and sequence files, and removes any duplicate entries (any two plasmids with the same name in either sequence file or meta file). This function is also transactional, but will not affect any file that contains no duplicate plasmids.

### addMetaField.py

#### Contains a function "addField" that takes in the name of the field that should be added to each plasmid, the default value for this field, and the name of the meta data file that contains the plasmid information. This function is transactional.