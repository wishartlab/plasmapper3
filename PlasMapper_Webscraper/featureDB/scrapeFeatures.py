from bs4 import BeautifulSoup
import requests

# Returns a string containing the given sequence with newlines added every 'interval' characters
def fastaSequence(sequence, interval):
    length = len(sequence)

    # no newlines needed
    if length <= interval:
        return sequence + "\n"
    
    # add newlines every 'interval' characters
    formattedSequence = ""
    currentIndex = 0
    for i in range(length // interval):
        formattedSequence += sequence[currentIndex : currentIndex + interval] + "\n"
        currentIndex += interval

    # add the last <= 'interval' characters
    if currentIndex < length:
        formattedSequence += sequence[currentIndex:] + "\n"
    return formattedSequence

# Scrapes the addgene page and writes fasta formatted features to a file called 'fileName'
def collectData(fileName):
    # get the page source
    source = requests.get('https://www.addgene.org/tools/reference/plasmid-features/').text

    # scrape the columns
    soup = BeautifulSoup(source, 'lxml')
    rows = soup.find_all("tr")
    file = open(fileName, 'w')

    # extract the name, type, and sequence of each feature
    for i in range(1, len(rows)):
        nameTypeCols = rows[i].find_all("td")
        sequence = rows[i].find(class_ = "copy-from well well-sm").text
        length = len(sequence)
        sequence = fastaSequence(sequence, 50)
        name = nameTypeCols[0].text
        type = nameTypeCols[1].text
        if type == "Promoter":
            file.write(">" + name + "[PRO]" + "{" + name + "}," + str(length) + " bases.\n" + sequence)
        elif type == "Origin":
            file.write(">" + name + "[ORI]" + "{" + name + "}," + str(length) + " bases.\n" + sequence)
        elif type == "Terminator":
            file.write(">" + name + "[TER]" + "{" + name + "}," + str(length) + " bases.\n" + sequence)
        elif type == "Regulatory":
            file.write(">" + name + "[REG]" + "{" + name + "}," + str(length) + " bases.\n" + sequence)
        elif type == "Feature":
            file.write(">" + name + "[OTH]" + "{" + name + "}," + str(length) + " bases.\n" + sequence)
        elif type == "Gene":
            file.write(">" + name + "[GEN]" + "{" + name + "}," + str(length) + " bases.\n" + sequence)
        elif type == "Primer":
            file.write(">" + name + "[PRI]" + "{" + name + "}," + str(length) + " bases.\n" + sequence)

collectData("features3Updated.fasta.nt")