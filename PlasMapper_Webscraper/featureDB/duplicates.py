import requests

# Writes a list of the names of features in queryFile that have duplicates in the API database to outputFile
# queryFile should be in FASTA format
# assumes the API is running on localhost:8000
def getDuplicates(queryFile, outputFile):
    queryFile = open(queryFile, 'r')
    outputFile = open(outputFile, 'w')
    queryFeatures = queryFile.read().split('>')

    # remove the empty string at index 0
    queryFeatures.pop(0)
    
    # for each query feature, check if it returns results against FSDSearch
    for feature in queryFeatures:
        sequenceStartIndex = feature.index("\n") + 1

        # get the name and sequence
        name = feature[:sequenceStartIndex]
        sequence = feature[sequenceStartIndex:]
        print("FEATURE:", feature)
        #print("SEQUENCE:", sequence)

        # pass the feature sequence to the API
        result = requests.post("http://localhost:8000/features", json = {"sequence" : sequence}).json()
        print(result)
        
        # write the names of any hits to duplicates.txt
        # a hit is defined as a feature that, when passed to the /features endpoint, returns more than one result
        if len(result["promoters"]) > 0:
            outputFile.write(name + " | " + str(result["promoters"]) + "\n")
        elif len(result["terminators"]) > 0:
            outputFile.write(name + " | " + str(result["terminators"]) + "\n")
        elif len(result["regulatorySequences"]) > 0:
            outputFile.write(name + " | " + str(result["regulatorySequences"]) + "\n")
        elif len(result["replicationOrigins"]) > 0:
            outputFile.write(name + " | " + str(result["replicationOrigins"]) + "\n")
        elif len(result["selectableMarkers"]) > 0:
            outputFile.write(name + " | " + str(result["selectableMarkers"]) + "\n")
        elif len(result["reporterGenes"]) > 0:
            outputFile.write(name + " | " + str(result["reporterGenes"]) + "\n")
        elif len(result["affinityTags"]) > 0:
            outputFile.write(name + " | " + str(result["affinityTags"]) + "\n")
        elif len(result["localizationSequences"]) > 0:
            outputFile.write(name + " | " + str(result["localizationSequences"]) + "\n")
        elif len(result["twoHybridGenes"]) > 0:
            outputFile.write(name + " | " + str(result["twoHybridGenes"]) + "\n")
        elif len(result["genes"]) > 0:
            outputFile.write(name + " | " + str(result["genes"]) + "\n")
        elif len(result["primers"]) > 0:
            outputFile.write(name + " | " + str(result["primers"]) + "\n")
        elif len(result["misc"]) > 0:
            outputFile.write(name + " | " + str(result["misc"]) + "\n")

getDuplicates("features3.fasta.nt", "duplicates.txt")