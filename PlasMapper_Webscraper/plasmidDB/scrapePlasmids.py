from bs4 import BeautifulSoup
import requests, math, json, re

pageSize = 50
homeUrl = "https://www.addgene.org"
baseUrl = "https://www.addgene.org/search/catalog/plasmids/?sticky=no&q=&expression=Mammalian+Expression&species=Synthetic&requests=100%2B+requests&page_size=" + str(pageSize)
addedNames = []

# The AddGene page does not properly implement pagination, so some results that you see on the first page will also be on the second page.
# Max page size is 50 and there is no predictable pattern to the order of results in the pagination.
# collectPlasmids continues to loop over all pages until all results have been accounted for.

# Scrapes the addgene page
# Writes JSON formatted plasmid sequences for each plasmid to a file called 'sequencesFileName'
# Writes JSON formatted plasmid metadata for each plasmid to a file called 'metaFileName'
def collectPlasmids(sequencesFileName, metaFileName):
    # get existing added names
    addedNames = getExistingNames(metaFileName)

    # get the page source
    source = requests.get(getUrl(1)).text

    # get the number of results
    soup = BeautifulSoup(source, 'lxml')
    numResultsString = soup.find(id = "search-num-results").text
    startIndex = numResultsString.index("of ") + 3
    endIndex = (numResultsString[startIndex:]).index(" ")
    numResults = int(numResultsString[startIndex : startIndex + endIndex])
    print("INITIALNUMRESULTS:", numResults)

    # find the number of pages
    numPages = math.ceil(numResults / pageSize)

    # sequence information
    sequenceFile = open(sequencesFileName, 'r')
    sequenceDicts = json.load(sequenceFile)
    sequenceFile.close()

    # meta information
    metaFile = open(metaFileName, 'r')
    metaDicts = json.load(metaFile)
    metaFile.close()

    # loop over each page
    while numResults > 0:
        page = 1
        while page <= numPages:
            url = getUrl(page)
            source = requests.get(url).text

            # all information (uncomment this section to collect both sequence, and meta data)
            numFound = collectInfo(source, metaDicts, sequenceDicts)
            numResults -= numFound
            print("NUMRESULTS:", numResults)

            # sequence information (uncomment this section to collect only sequence)
            #numFound = collectSequences(source, sequenceDicts)
            #numResults -= numFound
            #print("NUMRESULTS:", numResults)

            # meta information (uncomment this section to collect only meta data)
            #numFound = collectMeta(source, metaDicts)
            #numResults -= numFound
            #print("NUMRESULTS:", numResults)

            page += 1

    # open files
    sequencesFile = open(sequencesFileName, 'w')
    metaFile = open(metaFileName, 'w')
    
    # sequence information
    sequencesFile.write(json.dumps(sequenceDicts))

    # meta information
    metaFile.write(json.dumps(metaDicts))

# Collects all metadata and sequences for a given page source
# Saves to list of dictionaries 'metaDicts' and a list of dictionaries 'sequenceDicts, where each dictionary represents a plasmid
def collectInfo(source, metaDicts, sequenceDicts):
    # scrape the search results list
    soup = BeautifulSoup(source, 'lxml')
    resultsList = soup.find("ol").find_all("li")
    numFound = 0

    # for each result, get the details page
    for result in resultsList:
        # get name
        detailsUrl = homeUrl + result.find("a").get("href")
        detailsSource = BeautifulSoup(requests.get(detailsUrl).text, 'lxml')
        name = detailsSource.find("span", {"class" : "material-name"}).text
        print("NAME:", name)
        if name not in addedNames:
            addedNames.append(name)
            numFound += 1
        else:
            print("DUPLICATE****")
            continue
        
        # get sequence length
        sequencesUrl = detailsUrl + "sequences/"
        sequencesSource = BeautifulSoup(requests.get(sequencesUrl).text, 'lxml')
        fullSequencesSection = sequencesSource.find("section", {"id" : re.compile('(.*?full.*?)')})

        # if no full sequence available, dont append it to the dictionary
        if fullSequencesSection != None:
            sequence = fullSequencesSection.find("textarea", {"class" : "copy-from form-control"})
            sequence = sequence.text
            indexOfNewline = sequence.index("\n") + 1
            sequence = sequence[indexOfNewline:]
            sequence = sequence.replace("\n", "")
            sequence = sequence.strip()
            sequenceLength = len(sequence)

            # get backbone
            backbone = detailsSource.find("div", class_ = "field-label", text = re.compile('(.*?backbone.*?)')).parent.text
            if backbone != None and not ("Unknown" in backbone):
                startIndex = backbone.index("backbone") + 8
                endIndex = backbone.index("(Search Vector Database)")
                backbone = backbone[startIndex : endIndex].strip()
            else:
                print("no backbone")
                backbone = ""

            # get expression type
            expression = detailsSource.find("div", class_ = "field-label", text = re.compile('(.*?Vector type.*?)')).parent.text
            if expression != None:
                startIndex = expression.index("Vector type") + 11
                expression = expression[startIndex:].strip()
            else:
                print("no expression type")
                expression = ""

            # append a dictionary
            metaDicts.append({"name" : name, "sequenceLength" : sequenceLength, "backbone" : backbone, "features" : "", "supplier": "AddGene", "expression": expression})
            sequenceDicts.append({"name": name, "sequence": sequence})
        else:
            print("no full sequence data")
    
    return numFound

# Collects all sequences for a given page source
# Saves to list of dictionaries 'dicts', where each dictionary represents a plasmid
def collectSequences(source, dicts):
    # scrape the search results list
    soup = BeautifulSoup(source, 'lxml')
    resultsList = soup.find("ol").find_all("li")
    numFound = 0

    # for each result, get the details page
    for result in resultsList:
        # get name
        detailsUrl = homeUrl + result.find("a").get("href")
        detailsSource = BeautifulSoup(requests.get(detailsUrl).text, 'lxml')
        name = detailsSource.find("span", {"class" : "material-name"}).text
        print("NAME:", name)
        if name not in addedNames:
            addedNames.append(name)
            numFound += 1
        else:
            print("DUPLICATE****")
            continue
        
        # get sequence
        sequencesUrl = detailsUrl + "sequences/"
        sequencesSource = BeautifulSoup(requests.get(sequencesUrl).text, 'lxml')
        fullSequencesSection = sequencesSource.find("section", {"id" : re.compile('(.*?full.*?)')})

        # if no full sequence available, dont append it to the dictionary
        if fullSequencesSection != None:
            sequence = fullSequencesSection.find("textarea", {"class" : "copy-from form-control"})
            sequence = sequence.text
            indexOfNewline = sequence.index("\n") + 1
            sequence = sequence[indexOfNewline:]
            sequence = sequence.replace("\n", "")
            sequence = sequence.strip()

            # append a dictionary
            dicts.append({"name" : name, "sequence" : sequence})
        else:
            print("no full sequence data")
    
    return numFound

# Collects all metadata for a given page source
# Saves to list of dictionaries 'dicts', where each dictionary represents a plasmid
def collectMeta(source, dicts):
    # scrape the search results list
    soup = BeautifulSoup(source, 'lxml')
    resultsList = soup.find("ol").find_all("li")
    numFound = 0

    # for each result, get the details page
    for result in resultsList:
        # get name
        detailsUrl = homeUrl + result.find("a").get("href")
        detailsSource = BeautifulSoup(requests.get(detailsUrl).text, 'lxml')
        name = detailsSource.find("span", {"class" : "material-name"}).text
        print("NAME:", name)
        if name not in addedNames:
            addedNames.append(name)
            numFound += 1
        else:
            print("DUPLICATE****")
            continue
        
        # get sequence length
        sequencesUrl = detailsUrl + "sequences/"
        sequencesSource = BeautifulSoup(requests.get(sequencesUrl).text, 'lxml')
        fullSequencesSection = sequencesSource.find("section", {"id" : re.compile('(.*?full.*?)')})

        # if no full sequence available, dont append it to the dictionary
        if fullSequencesSection != None:
            sequence = fullSequencesSection.find("textarea", {"class" : "copy-from form-control"})
            sequence = sequence.text
            indexOfNewline = sequence.index("\n") + 1
            sequence = sequence[indexOfNewline:]
            sequence = sequence.replace("\n", "")
            sequence = sequence.strip()
            sequenceLength = len(sequence)

            # get backbone
            backbone = detailsSource.find("div", class_ = "field-label", text = re.compile('(.*?backbone.*?)')).parent.text
            if backbone != None and not ("Unknown" in backbone):
                startIndex = backbone.index("backbone") + 8
                endIndex = backbone.index("(Search Vector Database)")
                backbone = backbone[startIndex : endIndex].strip()
            else:
                print("no backbone")

            # get expression type
            expression = detailsSource.find("div", class_ = "field-label", text = re.compile('(.*?Vector type.*?)')).parent.text
            if expression != None:
                startIndex = expression.index("Vector type") + 11
                expression = expression[startIndex:].strip()
            else:
                print("no expression type")

            # append a dictionary
            dicts.append({"name" : name, "sequenceLength" : sequenceLength, "backbone" : backbone, "features" : "", "supplier": "AddGene", "expression": expression})
        else:
            print("no full sequence data")
    
    return numFound

def getUrl(pageNumber):
    return baseUrl + "&page_number=" + str(pageNumber)

def getExistingNames(file):
    f = open(file, 'r')
    existingPlasmids = json.load(f)
    resultsList = []
    for plasmid in existingPlasmids:
        resultsList.append(plasmid["name"])
    return resultsList

collectPlasmids("sequencesBatch16.json", "metadataBatch16.json")