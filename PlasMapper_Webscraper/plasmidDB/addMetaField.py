from email.policy import default
import json

# Add a new metadata field to each plasmid in metadata.json
def addField(fieldName, defaultValue, metaFileName):
    metaFile = open(metaFileName, 'r')
    meta = json.load(metaFile)
    metaFile.close()

    # loop over plasmid dictionaries in meta
    for plasmid in meta:
        if plasmid.get(fieldName) != None:
            raise Exception(fieldName + " field already exists!")
        else:
            plasmid[fieldName] = defaultValue

    # write the new meta dictionary to the metaFile (overwrite)
    metaFile = open(metaFileName, 'w')
    metaFile.write(json.dumps(meta))

addField("popularity", 0, "metadata.json")