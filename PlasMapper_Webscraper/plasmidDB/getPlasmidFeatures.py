from urllib import response
import requests, json

apiUrl = "http://127.0.0.1:7000/api/features" # the url of the get features given sequence endpoint

# Fills the 'features' attribute of all plasmids in metaFileName by passing their sequences in sequencesFileName to the API
def getPlasmidFeatures(sequencesFileName, metaFileName):
    # load dictionaries
    sequencesFile = open(sequencesFileName, 'r')
    metaFile = open(metaFileName, 'r')
    sequences = json.load(sequencesFile)
    meta = json.load(metaFile)
    sequencesFile.close()
    metaFile.close()

    # loop over plasmid dictionaries in meta
    for plasmid in meta:
        # get the sequence with the same name in sequences
        name = plasmid["name"]
        sequence = getSequence(name, sequences)
        response = requests.post(apiUrl, json = {"sequence" : sequence})
        print(response)
        # update features
        updateFeatures(response, plasmid)

    # write the new meta dictionary to the metaFile (overwrite)
    metaFile = open(metaFileName, 'w')
    metaFile.write(json.dumps(meta))

# Extracts all feature names to a comma separated string and updates the meta dictionary
def updateFeatures(response, plasmid):
    print(plasmid['name'])
    resultFeatureLists = response.json()
    featuresString = ""

    # loop over the feature list for each type of feature
    for featureList in resultFeatureLists.values():
        # loop over each feature in the list
        for feature in featureList:
            # append the name of the feature to the string
            featuresString += (feature['name'] + ',')

    # remove the trailing comma
    featuresString = featuresString[:len(featuresString) - 1]

    # update meta dictionary
    plasmid['features'] = featuresString

# Returns the sequence of a plasmid given the name
def getSequence(name, sequences):
    for plasmid in sequences:
        if plasmid["name"] == name:
            return plasmid["sequence"]

    # if no plasmid with the same name found, return None
    return None

# Removes any duplicated entries
def removeDuplicates(sequencesFileName, metaFileName):
    # load dictionaries
    sequencesFile = open(sequencesFileName, 'r')
    metaFile = open(metaFileName, 'r')
    sequences = json.load(sequencesFile)
    meta = json.load(metaFile)
    sequencesFile.close()
    metaFile.close()
    metaCount = 0
    sequencesCount = 0

    # find duplicates in sequences
    print("SEQUENCES DUPLICATES:", )
    for plasmid in sequences:
        if sequences.count(plasmid) > 1:
            sequencesCount += 1
            print(plasmid["name"], sequences.count(plasmid))
        while sequences.count(plasmid) > 1:
            sequences.remove(plasmid)

    # find duplicates in meta
    print("META DUPLICATES:", )
    for plasmid in meta:
        if meta.count(plasmid) > 1:
            metaCount += 1
            print(plasmid["name"], meta.count(plasmid))
        while meta.count(plasmid) > 1:
            meta.remove(plasmid)

    # write to file
    sequencesFile = open(sequencesFileName, 'w')
    metaFile = open(metaFileName, 'w')
    sequencesFile.write(json.dumps(sequences))
    metaFile.write(json.dumps(meta))

getPlasmidFeatures('../../ScraperSukanta/newSequences3.json', '../../ScraperSukanta/newMetadata3.json')
# removeDuplicates("sequences.json", "metadata.json")


